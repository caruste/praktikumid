package praktikum3;

import lib.TextIO;

public class ParooliOigsus {

	public static void main(String[] args) {

		// küsi ning paigalda parool
		System.out.println("Palun sisestage parool");
		String parool = TextIO.getlnString();
		String kParool = "I09-\"im2N";

		// kontrolli parooli õigsust
		while (!parool.equals(kParool)) {
			System.out.println("Parool on vale, palun sisestage õige parool");
			parool = TextIO.getlnString();
		}
		System.out.println("Parool on õige");

	}

}
