package praktikum3;

import lib.TextIO;

public class TehisIntellekt {

	public static void main(String[] args) {

		// küsi vanust 1 ja kontrolli
		System.out.println("Palun sisestage vanus 1");
		int vanus1 = TextIO.getInt();
		while (vanus1 < 0) {
			System.out.println("Vanus ei saa olla negatiivne!");
			vanus1 = TextIO.getInt();
		}

		// küsi vanust 2 ja kontrolli
		System.out.println("Palun sisestage vanus 2");
		int vanus2 = TextIO.getInt();
		while (vanus2 < 0) {
			System.out.println("Vanus ei saa olla negatiivne!");
			vanus2 = TextIO.getInt();
		}

		// tee tehted varem ära
		int vVahe1 = Math.abs(vanus2 - vanus1);
		int vVahe2 = Math.abs(vanus1 - vanus2);

		// kontrolli vanuste vahet
		if (vVahe1 > 10 || vVahe2 > 10) {
			System.out.println("Vanuste vahe on rohkem kui 10.");
		} else {
			if (vVahe1 >= 5 || vVahe2 >= 5) {
				System.out.println("Vanuste vahe on rohkem kui 5.");
			} else {
				if (vVahe1 < 5 || vVahe2 < 5) {
					System.out.println("Vanuste vahe on väiksem kui 5.");
				}
			}
		}
	}
}
