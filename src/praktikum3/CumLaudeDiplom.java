package praktikum3;

import lib.TextIO;

public class CumLaudeDiplom {

	public static void main(String[] args) {

		// küsi keskmist hinnet ja kontrolli
		System.out.println("Mis on sinu kaalutud keskhinne?");
		double hinne = TextIO.getFloat();

		while (hinne < 0 || hinne > 5) {
			System.out.println("Keskhinne on vale, palun sisestage uuest");
			hinne = TextIO.getDouble();
		}

		// küsi lõputöö hinnet ja kontrolli
		System.out.println("Mis on sinu lõputöö hinne?");
		double loputoo = TextIO.getFloat();

		while (loputoo < 0 || loputoo > 5) {
			System.out.println("Lõputöö hinne on vale, palun sisestage uuest");
			loputoo = TextIO.getDouble();
		}

		// kas saab diplomile?
		if (hinne > 4.5 && loputoo == 5) {
			System.out.println("Jah saad cum laude diplomile!");
		} else {
			System.out.println("Ei saa!");
		}
	}

}
