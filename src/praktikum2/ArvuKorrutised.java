package praktikum2;

import lib.TextIO;

public class ArvuKorrutised {

	 public static void main(String[] args) {
		 
		 //Arvude sisestamine
		 System.out.println("Palun sisestage kaks arvu");
		 
		 //defineerime muutujad
		 int arv1 = TextIO.getlnInt();
		 int arv2 = TextIO.getlnInt();
	     
	     //korrutame väärtused
	     int korrutis= arv1*arv2;
	     
	     //väljastame muutujad
	     System.out.println("Arvude korrutis on: "+korrutis);
	 }
}
