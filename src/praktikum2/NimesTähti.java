package praktikum2;

import lib.TextIO;

public class NimesTähti {

	public static void main(String[] args) {

		System.out.println("Mis on sinu nimi?");
		String nimi = TextIO.getlnString();

		String nimiS = nimi.replaceAll(" ", "");

		// nimi.replaceAll(" ", "");

		System.out.println("Sinu nime pikkus on: " + nimiS.length());

	}

}
