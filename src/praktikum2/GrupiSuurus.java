package praktikum2;

import lib.TextIO;

public class GrupiSuurus {

	public static void main(String[] args) {

		// inimeste arv
		System.out.println("Palun sisestage inimeste arv");
		int iArv = TextIO.getlnInt();

		System.out.println("Palun sisestage grupi suurus");
		int iGrupp = TextIO.getlnInt();

		// gruppide moodustamine
		int gArv = iArv / iGrupp;
		System.out.println(gArv + " on gruppide arv, inimesi jääb üle " + iArv % iGrupp);

	}

}
