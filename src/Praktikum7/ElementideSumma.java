package Praktikum7;

public class ElementideSumma {

	public static int summa(int[] massiiv) {
	    // meetodi sisu siia
		int summa=0;
		for (int i=0; i<massiiv.length;i++) {
			summa+=massiiv[i];
		}
		return summa;
	}

	public static void main(String[] args) {
	    // sedasi saab meetodi välja kutsuda
	    int summa = summa(new int[] {4, 3, 1, 7, -1});
	    System.out.println(summa);
	}

}
