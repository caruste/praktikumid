package Praktikum7;

import lib.TextIO;

public class ArvuTagastus {

	public static void main(String[] args) {
		
		int arvud[]= new int[10];
		
		for (int i= 0; i<10; i++) {
			System.out.println("Palun sisestage "+(i+1)+" arv");
			arvud[i]= TextIO.getlnInt();
		}
		
		System.out.println("Teie arvud tagurpidi on: ");
		for (int i= 9; i>=0; i--) {
			System.out.println(arvud[i]);
		}
		
	}

}
