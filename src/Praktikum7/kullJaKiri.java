package Praktikum7;

import lib.TextIO;

class kullJaKiri {

	public static void main(String[] args) {

		int raha = 100;
		System.out.println("Teil on hetkel 100 eurot.");

		while (raha != 0 || raha < 0) {
			System.out.println("Mis on Teie panus? (Maksimum panus on 25 eurot.)");
			int panus = TextIO.getlnInt();

			while (panus > raha) {
				System.out.println("Teil ei ole panuse jaoks piisavalt raha,");
				System.out.println("palun sisestage summa uuesti, hetkel on teile alles " + raha);
				panus = TextIO.getlnInt();
			}

			while (panus > 25 || panus < 0) {
				System.out.println("Palun sisestage õige panus.");
				panus = TextIO.getlnInt();
			}
			System.out.println("Teie panustasite " + panus + " eurot");

			int vise = (int) (Math.random() * 2);

			if (vise == 1) {
				System.out.println("Tuli kiri, võitsite tagasi topelt.");
				raha = raha + (panus * 2);
				System.out.println("Teil on hetkel raha " + raha);
			} else {
				System.out.println("Tuli kull, kaotasite panuse.");
				raha = raha - panus;
				System.out.println("Teil on hetkel raha " + raha);

			}
		}
	}

}
