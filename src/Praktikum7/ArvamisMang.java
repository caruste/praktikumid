package Praktikum7;

import lib.TextIO;

public class ArvamisMang {

	public static void main(String[] args) {

		System.out.println("Mis arvust Teie mõtlete?");
		int kasutajaArv = TextIO.getlnInt();
		int arvutiArv = arv(100);
		boolean sobib = (arvutiArv == kasutajaArv);

		if (sobib)
			System.out.println("Arvasite õige numbri");

		System.out.println(arvutiArv);
		while (kasutajaArv != arvutiArv) {

			if (kasutajaArv < arvutiArv) {
				System.out.println("Arv, mille sisestasite on väiksem");
			} else {
				System.out.println("Arv, mille sisestasite on suurem");
			}
			System.out.println("Sisestage uus arv");
			kasutajaArv = TextIO.getlnInt();
			if (kasutajaArv == arvutiArv) {
				System.out.println("Õige arv");
			}
		}
		
	}

	private static int arv(int suurus) {

		return (int) (Math.random() * suurus + 1);

	}

}
