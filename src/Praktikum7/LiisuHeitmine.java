package Praktikum7;

import lib.TextIO;

public class LiisuHeitmine {

	public static void main(String[] args) {
		
		System.out.println("Mitu nime sisestate?");
		int nimesid = TextIO.getlnInt();
		
		String nimed[] = new String[nimesid];
		
		for (int i= 0; i<nimesid; i++) {
			System.out.println("Sisestage "+(i+1)+". nimi: ");
			nimed[i]= TextIO.getlnString();
		}
//		for (int i= 0; i<nimed.length;i++) {
//			System.out.println(nimed[i]);
//		}
		
		int nimeArv= (int)(Math.random()*nimesid+1);
		System.out.println(nimed[nimeArv]);
		

	}

}
