package Praktikum7;

import lib.TextIO;

public class TagurpidiSona {

	public static void main(String[] args) {

		System.out.println("Palun sisestage sõna.");
		String sisend = TextIO.getlnString();
		System.out.println("Teie sõna tagurpidi on: \"" + tagurpidi(sisend) + "\"");

	}
	public static String tagurpidi(String sisend) {
		
		StringBuilder tagurPidi = new StringBuilder(sisend);
		tagurPidi.reverse();
		return tagurPidi.toString();
	}
}
