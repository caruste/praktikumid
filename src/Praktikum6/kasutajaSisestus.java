package Praktikum6;

import lib.TextIO;

public class kasutajaSisestus {
	
	public static void main(String[] args) {
		
		System.out.println("Sisesta hinne 1-5");
		int hinne = kasutajaSisestus(1, 5);
		System.out.println("Sisestatud hinne: "+ hinne);
	}

	public static int kasutajaSisestus(int min, int max) {

		int sisestus= TextIO.getlnInt();
		while (sisestus<min || sisestus>max) {
			System.out.println("Sisestasite vale hinde, palun sisestage uuesti.");
			sisestus=TextIO.getlnInt();
		}
		return sisestus;

	}

}
