package Praktikum6;

import lib.TextIO;

public class KullKiri {

	public static void main(String[] args) {
	
		System.out.println("Valige üks järgmistest: ");
		System.out.println("1 = kull");
		System.out.println("2 = kiri");
		int sisestus = TextIO.getlnInt();

		while (sisestus!=1 && sisestus !=2) {
			System.out.println("Sisestus on vale, palun kirjutage uuesti!");
			sisestus = TextIO.getlnInt();
		}
		
		int vastus;
		vastus = (int) (Math.random()*2);
		
		if (vastus == sisestus) {
			System.out.println("Arvasite õigesti!");
		} else {
			System.out.println("Arvasite valesti!");
		}	

	}

}
