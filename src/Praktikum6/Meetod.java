package Praktikum6;

import lib.TextIO;

public class Meetod {

	public static void main(String[] args) {
		
		System.out.println("Sisestage arv");
		int a = TextIO.getlnInt();
		int arvKuubis = kuup(a);
		System.out.println("Arv kuubis on: "+ arvKuubis);

	}

	public static int kuup(int i) {

		return (int) Math.pow(i, 3);
		
	}

}
