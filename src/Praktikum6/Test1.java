package Praktikum6;

import lib.TextIO;

class Test1 {

	public static void main(String[] args) {

		String kysimus = "Kas kull: 1, või kiri: 2?";
		if (kasutajaSisend(kysimus) == mynt()) {
			System.out.println("Arvasid õigesti");
		} else {
			System.out.println("Arvasid valesti");
		}
		

	}

	public static int kasutajaSisend(String kysimus) {
		System.out.println(kysimus);
		int sisend = TextIO.getlnInt();

		while (sisend != 1 && sisend != 2) {
			System.out.println("Sisestasite vale numbri, palun sisestage uuesti");
			sisend = TextIO.getlnInt();
		}

		return sisend;
	}

	public static int mynt() {
		int mynt;
		mynt = (int) (Math.random() * 2 + 1);
		return mynt;
	}

}
