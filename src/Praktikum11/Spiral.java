package Praktikum11;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Spiral extends Applet{
	
	private Graphics g; 
	
	public void paint(Graphics g) {
		this.g = g; 
		drawBackground();
		drawSpiral();
	}
	
	public void drawBackground() {
        int w = getWidth();
        int h = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);
    }
	
	private void drawSpiral() {
		
		g.setColor(Color.BLACK);
		int centerX = getWidth()/2;
		int centery = getHeight()/2;
		
		for (double deg = 0; deg < Math.PI*2; deg= deg+ 0.3){
			double r = 10+ deg*2;
			int x = (int) (r * Math.cos(deg));
			int y = (int) (r * Math.sin(deg));
			g.fillRect(x, y, (int)deg, (int)deg);
			
		}
			
	}

}
