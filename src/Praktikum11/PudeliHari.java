package Praktikum11;

import java.applet.Applet;
import java.awt.*;

public class PudeliHari extends Applet {
    /*
     * Ringjoone vo~rrand parameetrilisel kujul
     * x = r * cos(t)
     * y = r * sin(t)
     * t = -PI..PI
     */
    public void paint(Graphics g) {

        int r = 100; // Raadius
        int x, y;
        double t;

        // Kysime kui suur aken on?
        int w = getWidth();
        int h = getHeight();
        
        // Keskpunkt kõrguse ja laiuse kaudu
        
        int x0; // Keskpunkt x
        int y0; // keskpunkt y
        
        x0 = w/2;
        y0 = h/2;
        
        // Raadius kõrgus & laiuse kaudu
        
        int r1; // uus raadius
        // Kumb on väiksem, kõrgus või laius
        if (w<h) { 
        	r1=w;
        } else {
        	r1=h;
        }
        r = r1/2; //Uus raadius on täpselt ääreni 

        // Ta"idame tausta
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);

        // Joonistame
        g.setColor(Color.black);

        for (t = -Math.PI; t < Math.PI; t = t + Math.PI / 16) {
            x = (int) (r * Math.cos(t) + x0);
            y = (int) (r * Math.sin(t) + y0);
            g.drawLine(x0, y0, x, y);
        }
    }
}