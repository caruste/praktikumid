package Praktikum11;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class TopToBottom extends Applet {

	public void paint(Graphics g) {

		int h = getHeight();
		int w = getWidth();
		
		for (int y = 0; y<h; y++) {
			double concentrate = (double) y/h;
			concentrate = 1-concentrate;
			
			int a = (int) (concentrate*255);
			
			Color color = new Color(a,a,a);
			g.setColor(color);
			g.drawLine(0, y, w, y);
			
		}
		
		
//		int height = getHeight();
//		double colorTest = 255/height;
//		
//		for (int i = 0; i < getHeight(); i++) {
//			int cM = (int) colorTest*height; 
//			Color color = new Color(cM, cM, cM);
//			g.setColor(color);
//			g.drawLine(0, cM, getWidth(), i);
//		}
	}
}
