package Praktikum5;

import lib.TextIO;

public class HuvitavTabel {

	
	public static void main(String[] args) {

		//valesti
		
		System.out.println("Palun sisestage tabeli suurus.");
		int tabeliSuurus = TextIO.getlnInt();
		int i;
		int j;
		for (i = 0; i < tabeliSuurus; i++) {
			System.out.print("|");
			for (j = 0; j < tabeliSuurus; j++) {
				if (i == j || i + j == tabeliSuurus - 1) {
					System.out.println(j);
				}
			}
			System.out.println("|");
		}
		

	}

}
