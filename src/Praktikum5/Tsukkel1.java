package Praktikum5;

import lib.TextIO;

public class Tsukkel1 {

	public static void main(String[] args) {
		int i;
		int j;
		int k = 9;
		System.out.println("Palun sisestage tabeli suurus.");
		int tabeliSuurus = TextIO.getlnInt();
		// Esimene test
		// for (i=1;i<=10;i++) {
		// System.out.print(i+" ");
		// }

		// Teine test
		// for (i = 0; i <= 10; i++) {
		// if (i % 2 == 0) {
		// System.out.print(i);
		// }
		// }

		// Kolmas test
		// for (i=1;i<=30;i++) {
		// if (i%3==0) {
		// System.out.print(i+ " ");
		// }
		// }

		// Neljas test
		// for (i = 0; i < 10; i++) {
		// for (j = 0; j < 10; j++) {
		// if (i == j) {
		// System.out.print(1);
		// } else {
		// System.out.print(0);
		// }
		// }
		// System.out.println();
		// }

		// Viies test
		// Esimesed jooned
		
		trykiaaris(tabeliSuurus);
		
		for (i = 0; i < tabeliSuurus; i++) {
			System.out.print("|");
			for (j = 0; j < tabeliSuurus; j++) {
				if (i == j || i + j == tabeliSuurus - 1) {
					System.out.print("x" + " ");
				} else {
					System.out.print(0 + " ");
				}
			}
			System.out.println("|");
		}
		
		trykiaaris(tabeliSuurus);

	}

	private static void trykiaaris(int tabeliSuurus) {
		int i;
		for (i = 0; i < tabeliSuurus; i++) {
			System.out.print("--");
		}

		System.out.print("--");
		System.out.println();

	}
}
