package Praktikum14;

import java.util.ArrayList;

public class FailiKeskmine {

	public static void main(String[] args) {

		// 1. Lugeda sisse faili sisu (ArrayList<String>)

		ArrayList<String> numbers = FailiLugeja.failiRead("kala.txt");
		System.out.println(numbers);

		// 2. ArrayList<String> -> ArrayList<Double>

		ArrayList<Double> doubles = ToNumbers(numbers);

		// 3. ArrayList<Double -> keskmine
		Collections.
		
		Double keskmine = AritmeetilineKeskmine(doubles);
		System.out.println(keskmine);

	}

	public static ArrayList<Double> ToNumbers(ArrayList<String> numbers) {

		ArrayList<Double> numberDouble = new ArrayList<Double>();

		for (String line : numbers) {
			try {
				double nr = Double.parseDouble(line);
				numberDouble.add(nr);
			} catch (NumberFormatException e) {
				System.out.println("File is not a number: \n" + e.getMessage());
			} catch (Exception e) {
				System.out.println("Error, muu error: \n" + e.getMessage());
			}
		}
		return numberDouble;

	}

	public static Double AritmeetilineKeskmine(ArrayList<Double> numbers) {

		double addition = 0;

		for (int i = 0; i < numbers.size(); i++) {
			addition += numbers.get(i);
		}
		addition = addition / numbers.size();
		return addition;
	}

}
