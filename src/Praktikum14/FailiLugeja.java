package Praktikum14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class FailiLugeja {
	public static void main(String[] args) {
	    
	    // punkt tähistab jooksvat kataloogi
	    String kataloogitee = FailiLugeja.class.getResource(".").getPath();
		
	    // otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(kataloogitee + "kala.txt");
	
		ArrayList<String> array = failiRead("kala.txt");
		
	}
	
	  public static ArrayList<String> failiRead(String file) {

	    // punkt tähistab jooksvat kataloogi
	    String kataloogitee = FailiLugeja.class.getResource(".").getPath();
		
	    File theFile = new File(kataloogitee + file);
	    
//	    System.out.println(theFile);
	    
		ArrayList<String> lines = new ArrayList();

		try {

			BufferedReader in = new BufferedReader(new FileReader(theFile));
			String rida;

			while ((rida = in.readLine()) != null) {
				lines.add(rida);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error, muu error: \n" + e.getMessage());
		}
		return lines;
	}



}