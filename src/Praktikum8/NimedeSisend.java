package Praktikum8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class NimedeSisend {

	public static void main(String[] args) {

		ArrayList<String> nimed = new ArrayList<String>();
		nimed.add("Mati");

		Scanner scanner = new Scanner(System.in);
		System.out.println("Palun sisestage nimi");
		String input = scanner.nextLine();

		while (!input.equals("")) {
			nimed.add(input);
			System.out.println("Palun sisestage uus nimi");
			input = scanner.nextLine();
			
		} 
		
		Collections.sort(nimed);
		
		for (String nimi : nimed) {
			System.out.println(nimi);
		}

	}

}
