package Praktikum8;

import java.util.Scanner;

public class ArvudTagurpidi {

	public static void main(String[] args) {
		
		int[] arvud = new int[10];
		Scanner scanner = new Scanner(System.in);
		
		for (int i = 0; i<arvud.length; i++) {
			
			System.out.print("Palun sisestage " + (i+1) + " number: ");
			arvud[i] = scanner.nextInt();
			
		}

		for (int i = (arvud.length-1); i>=0; i--) {
			System.out.print(arvud[i]+" ");
			
		}
		
	}

}
