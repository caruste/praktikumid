package Praktikum8;

public class MaksimaalneElement {

	public static void main(String[] args) {

		int[] arvud = { 0, 2, 5, 12, 6, 15, 5, 9, 0, 10 };
		System.out.println(maksimum(arvud));

		int[][] mArvud = { { 0, 2, 5, 6, -1, 5, 9, 0, 10, 100 }, { 1, 5, 9, 90, 0, -1, -100 }, {1000}};
		System.out.println(maksimum(mArvud));
	}

	// meetod, mis leiab ühemõõtmelise massiivi maksimumi
	public static int maksimum(int[] massive) {
		int maksimum = 0;
		for (int i = 0; i < massive.length - 1; i++) {
			if (maksimum < massive[i + 1]) {
				maksimum = massive[i + 1];
			}
		}
		return maksimum;
	}

	// meetod, mis leiab maatriksi maksimumi
	public static int maksimum(int[][] maatriks) {
		int maksimum = 0;

		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maksimum < maatriks[i][j])
					maksimum = maatriks[i][j];

			}
		}
		return maksimum;
	}
}
